
from termios import CKILL
import numpy as np
from sklearn.model_selection import train_test_split


def convert_to_list(file_name: str, train_set: float, shuffle: bool):

    list_new = []
    list_dict_new = []
    open_file = open(file_name, 'r', encoding='utf-8')

    for read_line in open_file:
        if read_line != "\n":
            list_dict_new.append(read_line)
        else:
            list_new.append(list_dict_new)
            list_dict_new = []

    cut_dict = (train_test_split(
        list_new, train_size=train_set, shuffle=shuffle))

    return cut_dict


    # print(list_new)
'''Tách list'''


def convert_to_string(list_item: list):
    convert_item = ""
    for item_of_list_one in list_item:
        for item_of_list_two in item_of_list_one:
            convert_item = convert_item + item_of_list_two
        convert_item = convert_item + "/n"
    return convert_item


def write_file(file_name: str, list_result: str):
    open_file = open(file_name, "w", encoding="utf-8")
    open_file.write(list_result)
    open_file.close()


def split_train_test(file_name: str, train_set: float, train_file_name: str, test_file_name: str, shuffle: bool = False):
    train_items, test_items = convert_to_list(file_name, train_set, shuffle)
    # convert to string
    train_set = convert_to_string(train_items)
    test_set = convert_to_string(test_items)
    write_file(train_file_name, train_set)
    write_file(test_file_name, test_set)


split_train_test(file_name='data_set.conll', train_set=0.8,
                 train_file_name='train.conll', test_file_name='test.conll')


test_read = open('test.conll', 'r', encoding='utf-8')
print(test_read.readline())
train_read = open('train.conll', 'r', encoding='utf-8')
print(train_read.read())
